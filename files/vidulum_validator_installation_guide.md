# Vidulum Validator Installation Guide

<!-- MarkdownTOC autolink="true" -->

- [SECTION 0: Requirements](#section-0-requirements)
- [SECTION 1: System preparation](#section-1-system-preparation)
	- [Add dedicated user](#add-dedicated-user)
	- [Go deployment](#go-deployment)
		- [Download and extract repository](#download-and-extract-repository)
	- [Firewall Configuration](#firewall-configuration)
	- [systemd Service Configuration](#systemd-service-configuration)
- [SECTION 2: Build and Initiate Vidulum Node](#section-2-build-and-initiate-vidulum-node)
	- [Add Go environmental variables](#add-go-environmental-variables)
	- [Build Vidulum binaries](#build-vidulum-binaries)
	- [Vidulum Node Init](#vidulum-node-init)
	- [Start node](#start-node)
- [SECTION 3: Promote Full Node to Validator Role](#section-3-promote-full-node-to-validator-role)
	- [Create Wallet](#create-wallet)
	- [Create Validator](#create-validator)

<!-- /MarkdownTOC -->


# SECTION 0: Requirements
 - Ubuntu 20.04 LTS


**Minimum**
 - 2 CPUs
 - 4GB RAM
 - 200GB SSD

 **Recommended**
 - 4 CPUs
 - 8GB RAM
 - 200GB SSD


# SECTION 1: System preparation
**NOTE:** ALL TASKS IN **SECTION 1** HAVE TO BE PERFORMED AS **root**

## Add dedicated user

## Go deployment

### Download and extract repository
```bash
GOVER=$(curl https://go.dev/VERSION?m=text)
wget https://golang.org/dl/${GOVER}.linux-amd64.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf ${GOVER}.linux-amd64.tar.gz
```
**NOTE**: That will install latest version of Go


## Firewall Configuration
```bash
ufw limit ssh/tcp comment 'Rate limit for openssh server' 
ufw default deny incoming
ufw default allow outgoing
ufw allow 26656/tcp comment 'Cosmos SDK/Tendermint P2P (Vidulum Validator)'
ufw enable
```


## systemd Service Configuration
Create service file ***/lib/systemd/system/vidulum.service*** for **Vidulum Validator** with following content:
```bash
[Unit]
Description=Vidulum Validator
After=network.target

[Service]
Group=vidulum
User=vidulum
WorkingDirectory=/home/vidulum
ExecStart=/home/vidulum/.local/bin/vidulumd start
Restart=on-failure
RestartSec=3
LimitNOFILE=8192

[Install]
WantedBy=multi-user.target
```

Once file is created you can reload systemd configuration and enable service.
```bash
systemctl daemon-reload && systemctl enable vidulum.service
```


# SECTION 2: Build and Initiate Vidulum Node
**NOTE:** ALL TASKS IN **SECTION 2** HAVE TO BE PERFORMED AS **vidulum** USER CREATED IN FIRST PART OF THIS GUIDE


## Add Go environmental variables
Set of variables, which should be set for user(s) with need to build Go apps.

Add Golang specific variables to ```${HOME}/.profile``` 
```bash
# add environmental variables for Go
if [ -f "/usr/local/go/bin/go" ] ; then
    export GOROOT=/usr/local/go
    export GOPATH=${HOME}/go
    export GOBIN=$GOPATH/bin
    export PATH=${PATH}:${GOROOT}/bin:${GOBIN}
fi
```

Once modified and saved, reload ```${HOME}/.profile``` to set variables in current user session
```bash
. ~/.profile
```


## Build Vidulum binaries

Before we build binaries for Vidulum node/validator we create folder where binaries will be stored.
Ubuntu adds this folder to search path, when it exists, so we can easily run binaries in future when needed.

```bash
mkdir -p ${HOME}/.local/bin
. ~/.profile
```

Now clone GitHub repository with Vidulum source code, build binaries and place in correct folder.
```bash
git clone https://github.com/vidulum/mainnet && cd mainnet && make install
mv ${HOME}/go/bin/vidulumd ${HOME}/.local/bin
```

## Vidulum Node Init
```bash
vidulumd init NODE_NAME --chain-id vidulum-1
```
**NOTE:** Replace NODE_NAME with name you want to assign to your validator.

In ***${HOME}/.vidulum/config/config.toml*** fine line which starts with ***persistent_peers =*** and replace with following content
```bash
persistent_peers =“209688f5bccb88f6397a97cc11ab545a014aa559@137.184.92.115:26656,d45e9dd8878d7c22d59ded3557f61da37420a4c6@95.217.118.211:26656,cae7d9d21c1752300277eab72d861b0c6638b2e3@164.68.119.151:26656,7a44ea6ecb59b0e4bd01b58a75163ec64b164bb4@63.210.148.24:26656,3bf3d98dfd4000dd5ff8189882a9f96848b99b87@137.220.60.196:26656,057fa262fe2030cc6e9095dc52d15b79ffcb923d@142.115.20.25:26656”
```

Now it is time to download ***genesis.json*** file, which will allow node synchronization
```bash
wget https://raw.githubusercontent.com/vidulum/mainnet/main/genesis.json -o ${HOME}/.vidulum/configure
```


## Start node
Once node is configured you can start it and synchronize chain database.
```bash
sudo systemctl start vidulum.service
```


To keep watching logs generated by Vidulum node use command below.
```bash
journalctl -u vidulum -f
```


To check, if node is synchronized.
```bash
curl http://localhost:26657/status
```

You will see JSON output where you need to locate ***catching_up*** field. When it will have value ***true*** means node is still synchronizing. When value is ***false*** means node is fully synchronized. Then you can move on to creating validator.
```json
........
    },
    "sync_info": {
      "latest_block_hash": "39B4F3361E27EEC32605DA2F554FE2C64EAB41A78B593531CC5E11EEDE9AD67C",
      "latest_app_hash": "05E9931EAF0284B4024D5393B8878146C1CFE0329183EE6084BD4EF48507FA29",
      "latest_block_height": "544241",
      "latest_block_time": "2021-11-29T20:26:22.352845323Z",
      "earliest_block_hash": "5614A5F7D398CCAC49EFB255D1F92421891B725808E31421317BDD519D35F7CA",
      "earliest_app_hash": "E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      "earliest_block_height": "1",
      "earliest_block_time": "2021-10-23T00:00:00Z",
      "catching_up": false
    },
........
```



# SECTION 3: Promote Full Node to Validator Role
**NOTE:** ALL TASKS IN **SECTION 3** HAVE TO BE PERFORMED AS **vidulum** USER CREATED IN FIRST PART OF THIS GUIDE

In order to create validator you need to have Vidulum account and some funds, whcih can be delegated to validator.


## Create Wallet
In order to create Vidulum wallet we use binaries we compiled earlier.
```bash
vidulumd keys add WALLET_NAME --keyring-backend os
```
You will be asked to provide password, which will protect keyring.

Output of this command will be similar to presented below
```bash
- name: WALLET_NAME
  type: local
  address: vdl1hjhglrzggqtdhsh3ag8jp0cckmva5pe976jxel
  pubkey: '{"@type":"/cosmos.crypto.secp256k1.PubKey","key":"Anriv0TNrt1cz3+pSq2UDNiJQZINNlgtknousVlcujZ7"}'
  mnemonic: ""


**Important** write this mnemonic phrase in a safe place.
It is the only way to recover your account if you ever forget your password.

some words forming mnemonic seed will be placed here you have to write them down and keep them safe
```
**NOTE: _When you generate wallet in last line you will have line full of random words. This is mnemonic seed (allows to restore wallet). Write that down and keep safe_**


Now you can transfer some funds to your wallet. To check balance on your account:
```bash
vidulumd query bank balances vdl1hjhglrzggqtdhsh3ag8jp0cckmva5pe976jxel
```

Output will be similar to this:
```bash
balances:
- amount: "2000000"
  denom: uvdl
pagination:
  next_key: null
  total: "0"
```
**NOTE:** Denomiation presented by command is in uvdl. For your information 1vdl = 1000000uvdl.

## Create Validator
Now we can turn full node into validator using account and funds created in previous steps.

```bash
vidulumd tx staking create-validator \
    --commission-max-change-rate="0.1" \
    --commission-max-rate="0.8" \
    --commission-rate="0.1" \
    --amount="1000000uvdl" \
    --pubkey=$(vidulumd tendermint show-validator) \
    --website="https://your.website" \
    --details="Description of your validator." \
    --security-contact="contact@your.domain" \
    --moniker=NODE_NAME \
    --chain-id=vidulum-1 \
    --min-self-delegation="1" \
    --gas auto \
    --gas-adjustment=1.2 \
    --fees 200000uvdl \
    --from=WALLET_NAME \
    --keyring-backend os
```
**NOTE: _When you generate wallet in last line you will have line full of random words. This is mnemonic seed (allows to restore wallet). Write that down and keep safe_**


Once that is done you shoudl see your node listed here: https://explorers.vidulum.app/vidulum/staking

ENJOY! YOU JUST BECAME VIDULUM CHAIN VALIDATOR!

JOIN VIDULUM DISCORD WHERE YOU CAN CHAT WITH COMMUNITY AND OTHER VALIDATORS https://discord.gg/hne7Ccq
