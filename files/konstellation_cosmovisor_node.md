# Install Konstellation Validator/Node with Cosmovisor

<!-- MarkdownTOC autolink="true" -->

- [Installation](#installation)
	- [Create folder structure](#create-folder-structure)
	- [Build Cosmovisor](#build-cosmovisor)
	- [Build Konstellation binaries](#build-konstellation-binaries)
	- [Initialize Node](#initialize-node)
- [Running Node](#running-node)
	- [Add service](#add-service)
	- [Start service](#start-service)
- [Monitor status](#monitor-status)
- [Appendix](#appendix)
	- [Resources](#resources)
	- [Upgrade at block 1826999](#upgrade-at-block-1826999)

<!-- /MarkdownTOC -->

**NOTE:** Please note that for purpose of running Konstellation Validator/Node dedicated user has been created called ***konstellation***. All steps should be done in context of ***konstellation*** user. When requires ***sudo*** command is explicitly used, when ***root*** permissions are required. It is highly recommended that you install Konstellation service on normal non-root user account.


# Installation

**NOTE:** All steps are done as ***konstellation*** user.

In order to build Konstellation Node binaries you need to have Go 1.17+ installed and properly configured.

## Create folder structure
```bash
mkdir -p ${HOME}/.local/bin
mkdir -p ${HOME}/.knstld/cosmovisor/genesis/bin
mkdir -p ${HOME}/.knstld/cosmovisor/upgrades/v0.44/bin
```

## Build Cosmovisor
```bash
cd ${HOME}
git clone https://github.com/cosmos/cosmos-sdk && cd cosmos-sdk/cosmovisor/
make
cp cosmovisor ${HOME}/.local/bin
```

## Build Konstellation binaries

```bash
cd ${HOME}
git clone https://github.com/konstellation/konstellation && cd konstellation
git checkout v0.4.3
make install
mv ${HOME}/go/bin/knstld ${HOME}/.knstld/cosmovisor/genesis/bin
git checkout v0.5.0
make install
mv ${HOME}/go/bin/knstld ${HOME}/.knstld/cosmovisor/upgrades/v0.44/bin
```

## Initialize Node

```bash
${HOME}/.knstld/cosmovisor/genesis/bin/knstld unsafe-reset-all
```

Download genesis and initial config.
```bash
wget -O ${HOME}/.knstld/config/genesis.json https://raw.githubusercontent.com/Konstellation/konstellation/master/config/genesis.json
wget -O $HOME/.knstld/config/config.toml https://raw.githubusercontent.com/Konstellation/konstellation/master/config/config.toml
```

In the ${HOME}/.knstld/config/config.toml file you can change moniker for your node.
```bash
# A custom human readable name for this node
moniker = "<node_moniker>"
```

If you running validator it is highly recommended to define minimum gas price in  ***${HOME}/.knstld/config/app.toml*** to avoid node being spammed with transactions.
```bash
# The minimum gas prices a validator is willing to accept for processing a
# transaction. A transaction's fees must meet the minimum of any denomination
# specified in this config (e.g. 10udarc).
minimum-gas-prices = ""
```


# Running Node

## Add service
Create service file **/lib.systemd/system/konstellation.service** with apropriate parameters

```bash
sudo nano /lib/systemd/system/konstellation.service
```

**NOTE:** Below service configuration assumes that user ***konstellation*** has been created for Validator/Node. If you have different user, adjust file accordingly.
```
[Unit]
Description=Konstellation Node
After=network-online.target

[Service]
User=konstellation
Group=konstellation
ExecStart=/home/konstellation/.local/bin/cosmovisor start
Restart=always
RestartSec=3
LimitNOFILE=16384
Environment="DAEMON_NAME=knstld"
Environment="DAEMON_HOME=/home/konstellation/.knstld"
Environment="DAEMON_ALLOW_DOWNLOAD_BINARIES=false"
Environment="DAEMON_RESTART_AFTER_UPGRADE=true"
Environment="DAEMON_LOG_BUFFER_SIZE=512"
Environment="UNSAFE_SKIP_BACKUP=true"

[Install]
WantedBy=multi-user.target
```

## Start service
Reload ***systemd*** configuration, enable and start ***konstellation*** service.
```bash
sudo systemctl daemon-reload
sudo systemctl enable konstellation.service
sudo systemctl start konstellation.service
```


# Monitor status
To see progress of synchronization
```bash
journalctl -u konstellation -f
```
Command will keep displaying all activity done by ***konstellation.service***.


# Appendix

## Resources
[Konstellation GitHub Repository](https://github.com/Konstellation)


## Upgrade at block 1826999
When chain reaches block 1826999 Cosmovisor will automatically swap binaries to new version and synchronization will continue.
