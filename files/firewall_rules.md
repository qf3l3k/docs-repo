# Firewall Rules

## General
```bash
sudo ufw limit ssh/tcp comment 'Rate limit for openssh server' 
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw enable
```

## Monitoring
```bash
sudo ufw allow 9100/tcp comment 'Prometheus Node Exporter'
sudo ufw allow 9090/tcp comment 'Prometheus'
sudo ufw allow 3000/tcp comment 'Grafana'
```
