# Test: Sentinel bluenet-1 Node as Docker Container

<!-- MarkdownTOC autolink="true" -->

- [Sentinel bluenet-1 Node Container](#sentinel-bluenet-1-node-container)
	- [Check container status](#check-container-status)
	- [Check container response](#check-container-response)
	- [Test transaction](#test-transaction)

<!-- /MarkdownTOC -->


Test was done as in some cases contenerized Sentinel Testnet Node was acting strange.


# Testing environment
VPS instance with Ubuntu 20.04.3 LTS.
 - 1 core vCPU
 - 2GB of RAM
 - 20GB of disk


# Sentinel bluenet-1 Node Container
```bash
docker run -d \
-e P2P_PERSISTENT_PEERS="8cafbfbf541ec5757c5fd6bdac219ab2f06b38a2@95.216.241.248:26656,7bc7ff12d43cd1f3cc575f6fac8ba28950efa608@49.12.206.250:26656" \
-e CHAIN_ID=bluenet-1 \
--mount type=bind,source=$HOME/.sentinelhub,target=/root/.sentinelhub \
-p 26656:26656 \
-p 26657:26657 \
chandrastation/bluenet:v0.1.0
```

## Check container status
```bash
docker ps -a
CONTAINER ID   IMAGE                           COMMAND                  CREATED        STATUS        PORTS
              NAMES
e174e872ac07   chandrastation/bluenet:v0.1.0   "run.sh /bin/sh -c '…"   10 hours ago   Up 10 hours   1317/tcp, 8080/tcp, 9090/tcp, 0.0.0.0:26656-26657->26656-26657/tcp, :::26656-26657->26656-26657/tcp   boring_shockley
```


## Check container response
```bash
curl http://localhost:26657/status
```

```json
{
  "jsonrpc": "2.0",
  "id": -1,
  "result": {
    "node_info": {
      "protocol_version": {
        "p2p": "8",
        "block": "11",
        "app": "0"
      },
      "id": "8b1329ec0c63a9db9bd457e843b3d46e1d533e5c",
      "listen_addr": "tcp://0.0.0.0:26656",
      "network": "bluenet-1",
      "version": "v0.34.10",
      "channels": "40202122233038606100",
      "moniker": "my-omnibus-node",
      "other": {
        "tx_index": "on",
        "rpc_address": "tcp://0.0.0.0:26657"
      }
    },
    "sync_info": {
      "latest_block_hash": "9FE240D440ED110F13E22B0729554AE71D94A42E46DB7818CD24DDD5F7641F91",
      "latest_app_hash": "9999716A339DF4CD1787C93B306D7CAB062D3D91CD1A28FA0467705C985A4B0D",
      "latest_block_height": "90916",
      "latest_block_time": "2021-09-17T05:44:45.178814685Z",
      "earliest_block_hash": "DCB6CF0307601316653712AED36FBDE164786071C8C71A3B667549275C4B9C08",
      "earliest_app_hash": "E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      "earliest_block_height": "1",
      "earliest_block_time": "2021-09-11T12:30:00Z",
      "catching_up": false
    },
    "validator_info": {
      "address": "EC0E8B344545F580993A75648ECCF3BCE67FB783",
      "pub_key": {
        "type": "tendermint/PubKeyEd25519",
        "value": "6O0iuevFW/ZBRvXQAHSlYWZruEzV/ux3eSLbrfJFrhw="
      },
      "voting_power": "0"
    }
  }
}
```


## Test transaction
```bash
sentinelhub tx bank send qf3l3k-test sent16egmzmxpa97hlx4sgkvdaflm3s96fhqhfyrfvv 1000000000ublue --chain-id bluenet-1 --node http://localhost:26657 --gas auto --fees 200000ublue
```

https://explorer.bluenet.sentinel.co/transactions/921DBF25A9DAAA4EC6EBE191F6A2C9EC4A48AC559C34A10A9770361E9EFCE825


# Conclusion

No strange behavior observed. Node was processing transactions as requested.
