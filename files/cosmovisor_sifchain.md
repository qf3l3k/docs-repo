# Install Cosmovisor for Sifchain Validator/Node

<!-- MarkdownTOC autolink="true" -->

- [Pre-requisites](#pre-requisites)
	- [Create folders](#create-folders)
	- [Build Cosmovisor](#build-cosmovisor)
- [Add service](#add-service)
- [Monitor status](#monitor-status)
- [Appendix](#appendix)
	- [Resources](#resources)
	- [Build binaries for Sifchain 0.9.10](#build-binaries-for-sifchain-0910)
	- [Build binaries for Sifchain 0.9.12](#build-binaries-for-sifchain-0912)
	- [Build binaries for Sifchain 0.9.14](#build-binaries-for-sifchain-0914)

<!-- /MarkdownTOC -->

**NOTE:** Please note that for purpose of running Sifchain Validator/Node dedicated user has been created called ***sifchain***.

# Pre-requisites

## Create folders
```bash
mkdir -p ${HOME}/.local/bin
mkdir -p ${HOME}/.sifnoded/cosmovisor/genesis/bin
mkdir -p ${HOME}/.sifnoded/cosmovisor/upgrades
mkdir -p ${HOME}/.sifnoded/cosmovisor/upgrades/0.9.10/bin
mkdir -p ${HOME}/.sifnoded/cosmovisor/upgrades/0.9.12/bin
mkdir -p ${HOME}/.sifnoded/cosmovisor/upgrades/0.9.14/bin
```
Appropriate versions of Sifchain binaries must be placed in appropriate folders.


**NOTE:** All steps done as ***sifchain*** user.


## Build Cosmovisor
```bash
cd ${HOME}
git clone https://github.com/cosmos/cosmos-sdk && cd cosmos-sdk/cosmovisor/
make
cp cosmovisor ${HOME}/.local/bin
```
**NOTE:** all steps done as ***sifchain*** user.


# Add service
Create service file **/lib.systemd/system/sifchain.service** with following content:

```bash
sudo nano /lib/systemd/system/sifchain.service
```

**NOTE:** Below service configuration assumes that user ***sifchain*** has been created for Validator/Node. If you have different user, adjust file accordingly.
```bash
[Unit]
Description=Sifchain Validator (Cosmovisor)
After=network-online.target

[Service]
User=sifchain
Group=sifchain
ExecStart=/home/sifchain/.local/bin/cosmovisor start
Restart=always
RestartSec=3
LimitNOFILE=4096
Environment="DAEMON_NAME=sifnoded"
Environment="DAEMON_HOME=/home/sifchain/.sifnoded"
Environment="DAEMON_ALLOW_DOWNLOAD_BINARIES=false"
Environment="DAEMON_RESTART_AFTER_UPGRADE=true"
Environment="DAEMON_LOG_BUFFER_SIZE=512"
Environment="UNSAFE_SKIP_BACKUP=true"

[Install]
WantedBy=multi-user.target
```

Reload ***systemd*** configuration, enable and start ***sifchain*** service.
```bash
sudo systemctl daemon-reload
sudo systemctl enable sifchain.service
sudo systemctl start sifchain.service
```


# Monitor status
To see progress of synchronization
```bash
journalctl -u sifchain -f
```
Command will keep displaying all activity done by ***sifchain.service***.


# Appendix

## Resources
[Sifchain Network (GitHub Repository)](https://github.com/Sifchain/networks/blob/master/README.md) - contains link to appropriate network files (genesis, peers) and current chain snapshot.
[Sifchain Node (GitHub Repository)](https://github.com/Sifchain/sifnode)

## Build binaries for Sifchain 0.9.10
```bash
cd ${HOME}
git clone https://github.com/Sifchain/sifnode && cd sifnode && git checkout betanet-0.9.10
make install
```
**NOTE:** Binaries will be located in ${HOME}/go/bin

## Build binaries for Sifchain 0.9.12
```bash
git clone https://github.com/Sifchain/sifnode && cd sifnode && git checkout betanet-0.9.12
make install
```
**NOTE:** Binaries will be located in ${HOME}/go/bin

## Build binaries for Sifchain 0.9.14
```bash
git clone https://github.com/Sifchain/sifnode && cd sifnode && git checkout v0.9.14
make install
```
**NOTE:** Binaries will be located in ${HOME}/go/bin
