# IBC Relayer Setup - KiChain <-> Rizon 

<!-- MarkdownTOC autolink="true" -->

- [Environment](#environment)
	- [Full nodes](#full-nodes)
	- [Relayer](#relayer)
- [Pre-requisites](#pre-requisites)
	- [Create user](#create-user)
	- [Additional packages](#additional-packages)
- [Relayer installation](#relayer-installation)
	- [Rust installation](#rust-installation)
		- [Relayer deployment](#relayer-deployment)
- [Generating keys](#generating-keys)
	- [Relayer keys](#relayer-keys)
		- [KiChain keys](#kichain-keys)
		- [Rizon keys](#rizon-keys)
	- [Transaction keys](#transaction-keys)
		- [KiChain](#kichain)
		- [Rizon](#rizon)
- [Relayer configuration](#relayer-configuration)
	- [Start Hermes relayer](#start-hermes-relayer)
- [Create channels](#create-channels)
- [Relayer state](#relayer-state)
- [IBC transactions](#ibc-transactions)
- [Balance verification](#balance-verification)
- [Relayer logs](#relayer-logs)

<!-- /MarkdownTOC -->


# Environment

## Full nodes
In order to provide environment for relayer 2 full nodes were deployed on same server as Relayer:
 - Kichain (chain-id: kichain-t-4)
 - Rizon (chain-id: groot-01)

Operating system for server, which runs nodes and relayer is Ubuntu 20.04 LTS.

**NOTE:** Nodes were reconfigured in order to co-exist on same server and do not cause port conflicts. Each node is running on dedicated, separate user account for seurity reasons.

## Relayer
Hermes relayer version 0.7.0 will be used
 - Repository: https://github.com/informalsystems/ibc-rs
 - Documentation: https://hermes.informal.systems/


# Pre-requisites

## Create user
```bash
adduser relayer
```
**NOTE:** All Build and configuration steps with Hermes Relayer will be don in context of this user. Running service as normal non-privileged user is good security practice and same time provides environment separation from other services/applications running on server.


## Additional packages
```bash
sudo apt install make clang pkg-config libssl-dev build-essential git jq llvm libudev-dev -y
```

# Relayer installation

## Rust installation
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env
```


### Relayer deployment
```bash
git clone https://github.com/informalsystems/ibc-rs.git
cd ibc-rs
git checkout v0.7.0
cargo build --release --bin hermes
```

**NOTE:** Once relayer is built binaries are stores in *${HOME}/ibc-rs/target/release* folder. I moved binaries to *${HOME}/.local/bin* along with binaries for each chain: *kid* and *rizond*. Then logoff and logon, so search path will be added to *${HOME}/.local/bin* folder.


# Generating keys
For testing purposes 4 keys will be creates. Two keys for relayer (one in each chain) and two for transactions (one in each chain).

## Relayer keys
Those keys will be used by relayer to sign cross-chain transactions. There must be some small amount of funds available on each account to pay gas fees.

### KiChain keys
```bash
kid keys add relayer-kichain --output json > keys-kichain.json
```

Output in JSON file should look similar to this:
```json
{
    "name": "relayer-kichain",
    "type": "local",
    "address": "tki13z2zwtkx8qwrhtlgrzyc8tj0w65gnldhvkxlxh",
    "pubkey": "tkipub1addwnpepqg28tpfaq6pymnl9hmv7nfghd8hv3s54sjek0450njpgc2rwrpypum9q4en",
    "mnemonic": "MNEMONIC SEED"
}
```
**NOTE:** Account was funded with small amount of tki coins prior to running relayer.


### Rizon keys
```bash
rizond keys add relayer-rizon --output json > keys-rizon.json
````

Output in JSON file should look similar to this:
```json
{
    "name": "relayer-rizon",
    "type": "local",
    "address": "rizon13eyqfrh48f8wc2dvhg5eflteu7ae0c5nt405dq",
    "pubkey": "rizonpub1addwnpepqvasqcp8s06a55f8seje5t8r94n7gd2sdqttzcre4vu0snags93lxf6se0p",
    "mnemonic": "MNEMONIC SEED"
}
```
**NOTE:** Account was funded with small amount of atolo coins prior to running relayer.


## Transaction keys
For the purpose of sending coins between chains two user wallets are created. Those two accounts will be simulating standard user activities later.

### KiChain
```bash
kid keys add qf3l3k-ibc
```

```yaml
- name: qf3l3k-ibc
  type: local
  address: tki1qs7w33jahmzr3ahkkhngp34x3rr9q2qr3sxy0x
  pubkey: tkipub1addwnpepq2lv09gn6pme647p2qhvsar6t79tck9ty8633a4dxw2x5ad7js42uzj6ls3
  mnemonic: ""
  threshold: 0
  pubkeys: []


**Important** write this mnemonic phrase in a safe place.
It is the only way to recover your account if you ever forget your password.

MNEMONIC SEED
```


### Rizon
```bash
rizond keys add qf3l3k-ibc
```

```yaml
- name: qf3l3k-ibc
  type: local
  address: rizon1xz7ynv702s4manty0zv4303p8d25x8fmnp8dfx
  pubkey: rizonpub1addwnpepqd2g424szsqvx4m7j5p57q4e80526lvhw0cx33x2gcu8qky379c77g7x6eh
  mnemonic: ""
  threshold: 0
  pubkeys: []


**Important** write this mnemonic phrase in a safe place.
It is the only way to recover your account if you ever forget your password.

MNEMONIC SEED
```

# Relayer configuration

**All activity will be done in context of *hermes* user, which is account created specifically to run relayer.**

Default folder for Hermes is *${HOME}/.hermes*, so it needs ot be created
```bash
mkdir -p ${HOME}/.hermes
```

Configuration file *${HOME}/.hermes/config.toml*
```toml
[global]
strategy = 'packets'
log_level = 'info'

[telemetry]
enabled = true
host    = '127.0.0.1'
port    = 3001

[rest]
enabled = true
host    = '127.0.0.1'
port    = 3000

[[chains]]
id = 'kichain-t-4'
rpc_addr = 'http://127.0.0.1:26807'
grpc_addr = 'http://127.0.0.1:9090'
websocket_addr = 'ws://localhost:26807/websocket'
rpc_timeout = '10s'
account_prefix = 'tki'
key_name = 'relayer-kichain'
store_prefix = 'ibc'
max_gas = 2000000
gas_price = { price = 0.001, denom = 'utki' }
gas_adjustment = 0.1
clock_drift = '10s'
trusting_period = '14days'
trust_threshold = { numerator = '1', denominator = '3' }

[[chains]]
id = 'groot-011'
rpc_addr = 'http://127.0.0.1:26857'
grpc_addr = 'http://127.0.0.1:9290'
websocket_addr = 'ws://localhost:26857/websocket'
rpc_timeout = '10s'
account_prefix = 'rizon'
key_name = 'relayer-rizon'
store_prefix = 'ibc'
max_gas = 2000000
gas_price = { price = 0.001, denom = 'uatolo' }
gas_adjustment = 0.1
clock_drift = '10s'
trusting_period = '14days'
trust_threshold = { numerator = '1', denominator = '3' }
```

**NOTE:** Remember that ports for RPC services were changed in configuration of particular node as both nodes are running on same machine. By default both nodes would try to use same ports, which would cause conflict.


## Start Hermes relayer
That will start Hermes in current shell. Recommended is to run it in *tmux* or *screen*, so shell session can be detached and relayer can continue to work. Another option is to run relayer as a systemd service.

```bash
hermes start
```

# Create channels
Below command will create path between two chains indicated as parameters. Hermes will use configuration file created in earlier steps to connect to nodes for particular chains.

```bash
hermes create channel kichain-t-4 groot-011 --port-a transfer --port-b transfer
```

If all goes fine, output of the command should be similar to logs below.
```bash
Sep 06 20:07:13.435  INFO using default configuration from '/home/relayer/.hermes/config.toml'
Sep 06 20:07:13.867  INFO Creating new clients, new connection, and a new channel with order ORDER_UNORDERED
Sep 06 20:07:13.884  WARN [kichain-t-4] waiting for commit of tx hashes(s) 683E6673B28FCFEB3F0732D34AE8148FF1E6E3799B4659DDC9DF71810B48CDD7
Sep 06 20:07:21.902  INFO 🍭 [groot-011 -> kichain-t-4:07-tendermint-10]  => CreateClient(
    CreateClient(
        Attributes {
            height: Height {
                revision: 4,
                height: 220580,
            },
            client_id: ClientId(
                "07-tendermint-10",
            ),
            client_type: Tendermint,
            consensus_height: Height {
                revision: 0,
                height: 418623,
            },
        },
    ),
)

Sep 06 20:07:21.953  WARN [groot-011] waiting for commit of tx hashes(s) 7DBCA52DC042CA4497A9A1B5E0729E33A3F9C7E4974FC9A39B1E57B19FD7BDB8
Sep 06 20:07:26.964  INFO 🍭 [kichain-t-4 -> groot-011:07-tendermint-24]  => CreateClient(
    CreateClient(
        Attributes {
            height: Height {
                revision: 0,
                height: 418625,
            },
            client_id: ClientId(
                "07-tendermint-24",
            ),
            client_type: Tendermint,
            consensus_height: Height {
                revision: 4,
                height: 220580,
            },
        },
    ),
)

Sep 06 20:07:26.968  WARN [kichain-t-4] waiting for commit of tx hashes(s) C65EDA05D5F83D1CE3E21B774F92B2569603072B062573C366FBC16CC42FCB94
    kichain-t-4 => OpenInitConnection(
    OpenInit(
        Attributes {
            height: Height {
                revision: 4,
                height: 220582,
            },
            connection_id: Some(
                ConnectionId(
                    "connection-13",
                ),
            ),
            client_id: ClientId(
                "07-tendermint-10",
            ),
            counterparty_connection_id: None,
            counterparty_client_id: ClientId(
                "07-tendermint-24",
            ),
        },
    ),
)

Sep 06 20:07:33.832  WARN [kichain-t-4] waiting for commit of tx hashes(s) BF773A498CBC1240E040A3CFB45189C247D3BDECF28924A661CA78AC7542D86C
Sep 06 20:07:45.279  WARN [groot-011] waiting for commit of tx hashes(s) D85795377A19C0B3EF85CFABCABE42517C9A1E486720B385B5441427CE4F8D0F
    groot-011 => OpenTryConnection(
    OpenTry(
        Attributes {
            height: Height {
                revision: 0,
                height: 418629,
            },
            connection_id: Some(
                ConnectionId(
                    "connection-20",
                ),
            ),
            client_id: ClientId(
                "07-tendermint-24",
            ),
            counterparty_connection_id: Some(
                ConnectionId(
                    "connection-13",
                ),
            ),
            counterparty_client_id: ClientId(
                "07-tendermint-10",
            ),
        },
    ),
)

Sep 06 20:07:53.465  WARN [groot-011] waiting for commit of tx hashes(s) 81F800E3E3192498E55FFB562A81440E822FF971C6ABE24DDE95DC687D6585C5
Sep 06 20:08:06.105  WARN [kichain-t-4] waiting for commit of tx hashes(s) C1EDD2718C215086FB42E2979F50EC075227B93265B1EAB7C4A27FA7A9A6AB4D
    kichain-t-4 => OpenAckConnection(
    OpenAck(
        Attributes {
            height: Height {
                revision: 4,
                height: 220588,
            },
            connection_id: Some(
                ConnectionId(
                    "connection-13",
                ),
            ),
            client_id: ClientId(
                "07-tendermint-10",
            ),
            counterparty_connection_id: Some(
                ConnectionId(
                    "connection-20",
                ),
            ),
            counterparty_client_id: ClientId(
                "07-tendermint-24",
            ),
        },
    ),
)

Sep 06 20:08:15.046  WARN [groot-011] waiting for commit of tx hashes(s) C0770AAE19FB8962FA03F0FA5E06B63BAEBF48EF77E01CE505A7D0BC9C301B32
    groot-011 => OpenConfirmConnection(
    OpenConfirm(
        Attributes {
            height: Height {
                revision: 0,
                height: 418633,
            },
            connection_id: Some(
                ConnectionId(
                    "connection-20",
                ),
            ),
            client_id: ClientId(
                "07-tendermint-24",
            ),
            counterparty_connection_id: Some(
                ConnectionId(
                    "connection-13",
                ),
            ),
            counterparty_client_id: ClientId(
                "07-tendermint-10",
            ),
        },
    ),
)

   Connection handshake finished for [Connection {
    delay_period: 0ns,
    a_side: ConnectionSide {
        chain: ProdChainHandle {
            chain_id: ChainId {
                id: "kichain-t-4",
                version: 4,
            },
            runtime_sender: Sender { .. },
        },
        client_id: ClientId(
            "07-tendermint-10",
        ),
        connection_id: Some(
            ConnectionId(
                "connection-13",
            ),
        ),
    },
    b_side: ConnectionSide {
        chain: ProdChainHandle {
            chain_id: ChainId {
                id: "groot-011",
                version: 0,
            },
            runtime_sender: Sender { .. },
        },
        client_id: ClientId(
            "07-tendermint-24",
        ),
        connection_id: Some(
            ConnectionId(
                "connection-20",
            ),
        ),
    },
}]

Sep 06 20:08:19.782  WARN [kichain-t-4] waiting for commit of tx hashes(s) C35857E6D5A2C97B973EAD4AE6273A8384778A42755668503B9AFEC12D5E9A99
Sep 06 20:08:22.086  INFO done kichain-t-4 => OpenInitChannel(
    OpenInit(
        Attributes {
            height: Height {
                revision: 4,
                height: 220590,
            },
            port_id: PortId(
                "transfer",
            ),
            channel_id: Some(
                ChannelId(
                    "channel-54",
                ),
            ),
            connection_id: ConnectionId(
                "connection-13",
            ),
            counterparty_port_id: PortId(
                "transfer",
            ),
            counterparty_channel_id: None,
        },
    ),
)

Sep 06 20:08:22.086  INFO successfully opened init channel
Sep 06 20:08:29.226  WARN [groot-011] waiting for commit of tx hashes(s) 0F9108A705207AA8E91E999283594B22C6ED6C51AA144427D69033FA4917F862
done groot-011 => OpenTryChannel(
    OpenTry(
        Attributes {
            height: Height {
                revision: 0,
                height: 418635,
            },
            port_id: PortId(
                "transfer",
            ),
            channel_id: Some(
                ChannelId(
                    "channel-17",
                ),
            ),
            connection_id: ConnectionId(
                "connection-20",
            ),
            counterparty_port_id: PortId(
                "transfer",
            ),
            counterparty_channel_id: Some(
                ChannelId(
                    "channel-54",
                ),
            ),
        },
    ),
)

Sep 06 20:08:40.669  WARN [kichain-t-4] waiting for commit of tx hashes(s) A3C0DEB4F68CEB52FE99151F2658D7DD200A62596625A394695E5B2E433B6D6F                     [65/1438]
Sep 06 20:08:48.092  INFO done with ChanAck step kichain-t-4 => OpenAckChannel(
    OpenAck(
        Attributes {
            height: Height {
                revision: 4,
                height: 220594,
            },
            port_id: PortId(
                "transfer",
            ),
            channel_id: Some(
                ChannelId(
                    "channel-54",
                ),
            ),
            connection_id: ConnectionId(
                "connection-13",
            ),
            counterparty_port_id: PortId(
                "transfer",
            ),
            counterparty_channel_id: Some(
                ChannelId(
                    "channel-17",
                ),
            ),
        },
    ),
)

Sep 06 20:08:53.616  WARN [groot-011] waiting for commit of tx hashes(s) DF2E64BFC93C020F919C588090646D1D2B587C84AB67A0E3E6BE71BA67876751
Sep 06 20:09:00.447  INFO done groot-011 => OpenConfirmChannel(
    OpenConfirm(
        Attributes {
            height: Height {
                revision: 0,
                height: 418639,
            },
            port_id: PortId(
                "transfer",
            ),
            channel_id: Some(
                ChannelId(
                    "channel-17",
                ),
            ),
            connection_id: ConnectionId(
                "connection-20",
            ),
            counterparty_port_id: PortId(
                "transfer",
            ),
            counterparty_channel_id: Some(
                ChannelId(
                    "channel-54",
                ),
            ),
        },
    ),
)

Success: Channel {
    ordering: Unordered,
    a_side: ChannelSide {
        chain: ProdChainHandle {
            chain_id: ChainId {
                id: "kichain-t-4",
                version: 4,
            },
            runtime_sender: Sender { .. },
        },
        client_id: ClientId(
            "07-tendermint-10",
        ),
        connection_id: ConnectionId(
            "connection-13",
        ),
        port_id: PortId(
            "transfer",
        ),
        channel_id: Some(
            ChannelId(
                "channel-54",
            ),
        ),
    },
    b_side: ChannelSide {
        chain: ProdChainHandle {
            chain_id: ChainId {
                id: "groot-011",
                version: 0,
            },
            runtime_sender: Sender { .. },
        },
        client_id: ClientId(
            "07-tendermint-24",
        ),
        connection_id: ConnectionId(
            "connection-20",
        ),
        port_id: PortId(
            "transfer",
        ),
        channel_id: Some(
            ChannelId(
                "",
            ),
        ),
    },
    connection_delay: 0ns,
    version: Some(
        "ics20-1",
    ),
}                    
```

# Relayer state
In order to check relayer state specific signal needs to be sent to process.

First identify Hermes' PID.
```bash
ps aux | grep hermes
```
Use PID from previous step to send signal to relayer, which will force it to display current state.
```bash
kill -SIGUSR1 <HERMES_PID>
```

Once Hermes receives signal it will show state of clients and channels. State will be displayed on screen where Hermes is running or written to log file.
```bash
Sep 07 08:08:38.686  INFO Dumping state (triggered by SIGUSR1)
Sep 07 08:08:38.709  INFO
Sep 07 08:08:38.709  INFO * Chains: groot-011, kichain-t-4
Sep 07 08:08:38.709  INFO * Client workers:
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-10 (id: 18)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-2 (id: 13)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-33 (id: 2)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-36 (id: 3)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-37 (id: 4)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-39 (id: 5)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-40 (id: 6)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-41 (id: 7)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-43 (id: 8)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-51 (id: 9)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-58 (id: 42)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-60 (id: 44)
Sep 07 08:08:38.709  INFO   - client::groot-011->kichain-t-4:07-tendermint-9 (id: 22)
Sep 07 08:08:38.709  INFO   - client::kichain-t-4->groot-011:07-tendermint-17 (id: 10)
Sep 07 08:08:38.709  INFO   - client::kichain-t-4->groot-011:07-tendermint-18 (id: 11)
Sep 07 08:08:38.709  INFO   - client::kichain-t-4->groot-011:07-tendermint-20 (id: 15)
Sep 07 08:08:38.709  INFO   - client::kichain-t-4->groot-011:07-tendermint-24 (id: 20)
Sep 07 08:08:38.709  INFO * Packet workers:
Sep 07 08:08:38.709  INFO   - packet::channel-0/transfer:kichain-t-4->groot-011 (id: 38)
Sep 07 08:08:38.709  INFO   - packet::channel-12/transfer:groot-011->kichain-t-4 (id: 32)
Sep 07 08:08:38.709  INFO   - packet::channel-13/transfer:groot-011->kichain-t-4 (id: 25)
Sep 07 08:08:38.709  INFO   - packet::channel-14/transfer:groot-011->kichain-t-4 (id: 24)
Sep 07 08:08:38.709  INFO   - packet::channel-14/transfer:kichain-t-4->groot-011 (id: 40)
Sep 07 08:08:38.709  INFO   - packet::channel-16/transfer:groot-011->kichain-t-4 (id: 14)
Sep 07 08:08:38.709  INFO   - packet::channel-17/transfer:groot-011->kichain-t-4 (id: 19)
Sep 07 08:08:38.709  INFO   - packet::channel-17/transfer:kichain-t-4->groot-011 (id: 33)
Sep 07 08:08:38.709  INFO   - packet::channel-18/transfer:groot-011->kichain-t-4 (id: 21)
Sep 07 08:08:38.709  INFO   - packet::channel-19/transfer:groot-011->kichain-t-4 (id: 43)
Sep 07 08:08:38.709  INFO   - packet::channel-41/transfer:kichain-t-4->groot-011 (id: 35)
Sep 07 08:08:38.709  INFO   - packet::channel-46/transfer:kichain-t-4->groot-011 (id: 16)
Sep 07 08:08:38.709  INFO   - packet::channel-50/transfer:kichain-t-4->groot-011 (id: 26)
Sep 07 08:08:38.709  INFO   - packet::channel-51/transfer:kichain-t-4->groot-011 (id: 12)
Sep 07 08:08:38.709  INFO   - packet::channel-54/transfer:kichain-t-4->groot-011 (id: 17)
Sep 07 08:08:38.710  INFO   - packet::channel-56/transfer:kichain-t-4->groot-011 (id: 23)
Sep 07 08:08:38.710  INFO   - packet::channel-80/transfer:kichain-t-4->groot-011 (id: 41)
Sep 07 08:08:38.710  INFO   - packet::channel-81/transfer:kichain-t-4->groot-011 (id: 45)
```


# IBC transactions

```bash
hermes tx raw ft-transfer groot-011 kichain-t-4  transfer channel-14 1 -o 1 -n 1 -d utki
Sep 07 08:33:56.951  INFO using default configuration from '/home/relayer/.hermes/config.toml'
Sep 07 08:33:57.384  WARN [kichain-t-4] waiting for commit of tx hashes(s) CC80A5FD0679C08737D243B2C5629DBC2248B4B7C01C2F0654973DC7E78B1F39
Success: [
    SendPacket(
        SendPacket {
            height: Height {
                revision: 4,
                height: 227821,
            },
            packet: PortId("transfer") ChannelId("channel-14") Sequence(6),
        },
    ),
]
```
https://api-challenge.blockchain.ki/txs/CC80A5FD0679C08737D243B2C5629DBC2248B4B7C01C2F0654973DC7E78B1F39


```bash
kid tx ibc-transfer transfer transfer channel-54 rizon1xz7ynv702s4manty0zv4303p8d25x8fmnp8dfx 111899utki --node http://localhost:26807 --from qf3l3k-ibc --chain-id kichain-t-4
```
https://api-challenge.blockchain.ki/txs/A53AA8978397AEAE1017788DC0ADAD01ABB2299614CC13EE0844679978C71E04


```bash
rizond tx ibc-transfer transfer transfer channel-17 tki1qs7w33jahmzr3ahkkhngp34x3rr9q2qr3sxy0x 111899uatolo --node http://localhost:26857 --from qf3l3k-ibc --chain-id groot-011
```
https://testnet.mintscan.io/rizon/txs/41821A14290527F73F198A63886E4CF0A12667AB0C471901C8643F7101573815


# Balance verification

## Rizon
```bash
rizond query bank balances rizon1xz7ynv702s4manty0zv4303p8d25x8fmnp8dfx --node http://localhost:26857
```                                            

```yaml
balances:                                                                       
- amount: "111899"                                                              
  denom: ibc/F9C59B0551E01311BA3B583291BE89195E31F26E11D65CE5218DFE48A613847A   
- amount: "3895194"                                                             
  denom: uatolo                                                                 
pagination:                                                                     
  next_key: null                                                                
  total: "0"                                                                                                                                           
```

## KiChain
```bash
kid query bank balances tki1qs7w33jahmzr3ahkkhngp34x3rr9q2qr3sxy0x --node http://localhost:26807
```

```yaml
balances:
- amount: "111899"
  denom: ibc/EC7742B80FA5B962EC5BB4ECEF96E0AB5C454B57E2A0BBC94CEC315F52861038
- amount: "1777251"
  denom: utki
pagination:
  next_key: null
  total: "0"
```


# Relayer logs
Example logs from relayer.
```bash
Sep 07 08:11:07.021  INFO [kichain-t-4:transfer/channel-51 -> groot-011] finished scheduling pending packets clearing height=Some(Height { revision: 4, height: 227599 })
Sep 07 08:11:07.026  INFO [kichain-t-4:transfer/channel-80 -> groot-011] finished scheduling pending packets clearing height=Some(Height { revision: 4, height: 227599 })
Sep 07 08:11:07.028  INFO [kichain-t-4:transfer/channel-46 -> groot-011] finished scheduling pending packets clearing height=Some(Height { revision: 4, height: 227599 })
Sep 07 08:11:07.029  INFO [kichain-t-4:transfer/channel-41 -> groot-011] finished scheduling pending packets clearing height=Some(Height { revision: 4, height: 227599 })
Sep 07 08:11:07.031  INFO [kichain-t-4:transfer/channel-0 -> groot-011] scheduling op. data with 1 msg(s) for Destination (height 4-227600)
Sep 07 08:11:07.032  INFO [kichain-t-4:transfer/channel-0 -> groot-011] finished scheduling pending packets clearing height=Some(Height { revision: 4, height: 227599 })
Sep 07 08:11:07.035  INFO [kichain-t-4:transfer/channel-0 -> groot-011] relay op. data of 1 msgs(s) to Destination (height 4-227600), delayed by: 3.278389ms [try 1/5]
Sep 07 08:11:07.035  INFO [kichain-t-4:transfer/channel-0 -> groot-011] prepending Destination client update @ height 4-227600
Sep 07 08:11:07.124  INFO [kichain-t-4:transfer/channel-0 -> groot-011] assembled batch of 2 message(s)
Sep 07 08:11:07.134  INFO [Async~>groot-011] response(s): 1; Ok:A17E540E660ED13A9C254477F2234B3C717A2F3BE940D1D98C2B59E6403A7B86

Sep 07 08:11:07.134  INFO [kichain-t-4:transfer/channel-0 -> groot-011] success
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-13 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-12 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-19 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-16 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-18 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-17 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-20 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.144  INFO [groot-011:transfer/channel-14 -> kichain-t-4] clearing pending packets height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.152  INFO [groot-011:transfer/channel-13 -> kichain-t-4] finished scheduling pending packets clearing height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.153  INFO [groot-011:transfer/channel-12 -> kichain-t-4] finished scheduling pending packets clearing height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.153  INFO [groot-011:transfer/channel-16 -> kichain-t-4] finished scheduling pending packets clearing height=Some(Height { revision: 0, height: 425099 })
Sep 07 08:16:10.154  INFO [groot-011:transfer/channel-19 -> kichain-t-4] finished scheduling pending packets clearing height=Some(Height { revision: 0, height: 425099 }$
```
