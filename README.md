# Guides


## Development environment
 - **[Go installation](https://gitlab.com/qf3l3k/guides/-/blob/main/files/go_install.md)** - steps how to install Go with all environmental variables to avoid any issues with additional packages and build
 - **[Node.js installation](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/node_install.md)** - steps to install Node.js on Debian/Ubuntu. Abstract from documentation.
 - **[Yarn installation](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/yarn_install.md)** - steps to install Yarn pakcage manager on Ubuntu.
 - **[Rust installation](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/rust_install.md)** - steps to install Rust on Linux as well as other components for Rust.
 

## System 
 - **[Firewall Rules](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/firewall_rules.md)** - list of common firewall rules for general use and service specific.
 - **[journalctl configuration](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/journal_ctl.md)** - tips how to configure journalctl to optimize system resources usage by this service
 - **[systemd service template](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/systems_service_template.md)** - example configuration file for systemd service


## Sentinel dVPN
 - **[Install Sentinel dVPN Node on Windows Subsystem for Linux](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/sentinel-dvpn-node-wsl.md)** - detailed guide how to prepare WSL instance and Linux kernel to host dVPN node.
 - **[Sentinel bluenet-1 Node as Docker container](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/testing_bluenet-1_docker.md)** - Tests to run Sentinel node as container. Mostly to evaluate if node behaves correctly and works without any issues when contenerized.


## Sifchain
  - **[Sifchain Full Node Installation (chain-id: sifchain-1)](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/sifchain_full_node.md)** - detailed guide how to install and synchronize from scratch full node for sifchain-1 chain.
 - **[Install Cosmovisor for Sifchain Validator/Node](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/cosmovisor_sifchain.md)** - step-by-step guide to deploy Cosmovisor for Sifchain Validator/Node


## Vidulum
  - **[Vidulum Validator Deployment Guide](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/vidulum_validator_installation_guide.md)** - defailed guide describing how to prepare system environment and deploy Vidulum full node and then promote it to validator role.

## IBC Relayer
 - **[IBC Relayer Setup (kichain-t-4 <-> groot-011)](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/ibc_hermes_kichain-t-4_groot-011.md)** - Hermes Relayer setup guide created as part of the KiChain challenge. Relays packets between KiChain kichain-t-4 testnet and Rizon groot-011 testnet.
 - **[IBC Relayer Setup (bitsong-2-devnet-2 <-> bluenet-1)](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/ibc_hermes_bitsong-2-devnet-2.md)** - Hermes Relayer setup between BitSong testnet and Sentinel testnet. Created as part of BitSong testing before mainnet migration and preparation for live relayer deployment in mainnet.
 - **[IBC Relayer Setup (neuron-1 <-> bluenet-1)](https://gitlab.com/qf3l3k/docs-repo/-/blob/main/files/ibc_hermes_neuron-1_bluenet-1.md)** - Hermes Relayer setup between GameHub testnet and Sentinel testnet. Created as part of GameHub testnet contest and preparation for mainnet.


## Donations
If you find anything useful and want to support future development, feel free to donate:

**BTC:** bc1qt5zdlnan6al9h504g693at434k490q4mkrhx6n<br>
**LTC:** LhmYS1mPk6UsWSJbjyETTVsGfJPmqWSuZP<br>
**ETH:** 0xCcec245D2685e6344F8a93BCa9d91E54707B980D<br>
**ATOM:** cosmos1cxn4n5283kdyd4p2lkae4snnszt9zcpf3kl6m4<br>
**ICON:** hx35758899bddc4e0bd198cfa34e286d6bb8e4a9ce<br>
**ZEC:** t1KUbcqm9FkPcmLrjdpGebQQtX2qVdUWKRm
